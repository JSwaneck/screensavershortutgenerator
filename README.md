# Screeensaver Shortcut Generator

This application is designed to fix a Windows 10 bug where picture screensavers would not shuffle. This generates a folder full of shortcuts to a large picture file tree and randomizes it periodically.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine.

### Prerequisites

1. A windows computer with the above bug

### Installing

1. Download this repo onto your computer
2. Create a folder to add photo shortcuts
3. Edit the FileManager/Data.json to reflect your source photos path and shortcuts path 
4. Build the project using your favorite .NET compiler/IDE
5. Build the project and run the executable GUI
6. Point the Windows screensaver slideshow to the shortcuts folder 
7. See Scheduled Shuffling
8. Enjoy!

### Scheduled Shuffling

Add the file name scrambler to Windows Task Scheduler to shuffle the screensaver periodically

## Credits

* **Jacob Swaneck**

Suggestions? Let me know!

jpswaneck@gmail.com