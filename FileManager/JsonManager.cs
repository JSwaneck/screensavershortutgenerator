using System;
using System.IO;
using System.Runtime.Remoting.Messaging;
using Newtonsoft.Json;

namespace FileManager
{
    public struct JsonData
    {
        public string FamilyPhotosPath;
        public string DestinationPath;
    }
    
    public static class JsonManager
    {
        public static void GenerateJson()
        {
            JsonData defaultData = new JsonData();
            defaultData.FamilyPhotosPath = "";
            defaultData.DestinationPath = "";
            WriteJson(defaultData);
        }

        public static void WriteJson(JsonData data)
        {
            string jsonText = JsonConvert.SerializeObject(data);
            if (File.Exists("Data.json"))
            {
                File.Delete("Data.json");
            }

            using (var newJson = File.CreateText("Data.json"))
            {
                newJson.Write(jsonText);
            }
        }
        
        public static JsonData? GetJson()
        {
            int RetryCount = 2;
            for (int i = 0; i < RetryCount; i++)
            {
                
                try
                {
                    var fileText = File.ReadAllText("Data.json");
                    return JsonConvert.DeserializeObject<JsonData>(fileText);
                }
                catch (Exception e)
                {
                    GenerateJson();
                }
            }
            return null;
        }
    }
}