using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace FileManager
{
    public static class FileIO
    {
        public static int FileCount(string path)
        {
            return GetFiles(path).Count();
        }
        
        
        public static void ShuffleDest(string path)
        {
            if (!Directory.Exists(path)) return;
            var files = FileIO.GetFiles(path, false);
            FileIO.ScrambleFileNames(files, path);
        }
        
        public static IEnumerable<string> GetFiles(string path, bool recursive = true)
        {
            Queue<string> queue = new Queue<string>();
            queue.Enqueue(path);
            while (queue.Count > 0)
            {
                path = queue.Dequeue();
                if (recursive)
                {
                    try
                    {
                        foreach (string subdir in Directory.GetDirectories(path))
                        {
                            queue.Enqueue(subdir);
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.Error.WriteLine(ex);
                    }   
                }

                string[] files = null;
                try
                {
                    files = Directory.GetFiles(path);
                }
                catch (Exception ex)
                {
                    Console.Error.WriteLine(ex);
                }

                if (files != null)
                {
                    foreach (var file in files)
                    {
                        yield return file;
                    }
                }
            }
        }

        private const int MaxRetryCount = 5;
        
        public static void ScrambleFileNames(IEnumerable<string> filePaths, string destPath)
        {
            var count = 0;
            foreach (var filePath in filePaths)
            {
                count++;
                Exception currentException = null;
                int currentRetryCount = 0;
                bool didSucceed = false;
                while (!didSucceed && currentRetryCount < MaxRetryCount)
                {
                    try
                    {
                        File.Move(filePath, destPath + "\\" + Guid.NewGuid() + ".lnk");
                        didSucceed = true;
                    }
                    catch (Exception e)
                    {
                        currentException = e;
                    }
                }

                if (count > 200000)
                {
                    return; //Testing in case the GetFiles pulls new ones that were renamed
                }
            }
        }    
    }
}