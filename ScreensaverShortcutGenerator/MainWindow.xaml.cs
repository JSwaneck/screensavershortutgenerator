﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using IWshRuntimeLibrary;
using System.Windows.Forms;
using FileManager;

namespace ScreensaverShortcutGenerator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        private JsonData _pathData = new JsonData();
        
        public MainWindow()
        {
            InitializeComponent();
            var initialJsonData = JsonManager.GetJson();
            _pathData.DestinationPath = "";
            _pathData.FamilyPhotosPath = "";
            if (initialJsonData == null) return;
            if (Directory.Exists(initialJsonData.Value.FamilyPhotosPath))
            {
                _pathData.FamilyPhotosPath = initialJsonData.Value.FamilyPhotosPath;
                RootPathTextBox.Text = initialJsonData.Value.FamilyPhotosPath;
                UpdateCountLabel();
            }
            if (Directory.Exists(initialJsonData.Value.DestinationPath))
            {
                _pathData.DestinationPath = initialJsonData.Value.DestinationPath;
                DestPathTextBox.Text = initialJsonData.Value.DestinationPath;
                //TODO Enable shuffle button
            }
        }

        private void CreateShortcuts()
        {
            ExecuteButton.IsEnabled = false;
            InfoLabel.Content = "Running";
            var dotCount = 0;
            var lastSecond = DateTime.Now.Second;
            try
            {
                if (!Directory.Exists(_pathData.FamilyPhotosPath))
                {
                    InfoLabel.Content = "Invalid source/family folder path";
                    return;
                }

                if (!Directory.Exists(_pathData.DestinationPath))
                {
                    InfoLabel.Content = "Invalid destination path";
                    return;
                }

                if (FileIO.FileCount(_pathData.DestinationPath) != 0)
                {
                    InfoLabel.Content = "Destination folder must be empty";
                    return;
                }

                var shell = new WshShell();
                var shortcutCounter = 0;
                var errorCounter = 0;
                foreach (var filePath in FileIO.GetFiles(_pathData.FamilyPhotosPath))
                {
                    try
                    {
                        var shortcut =
                            (IWshShortcut) shell.CreateShortcut($"{_pathData.DestinationPath}\\shortcut{shortcutCounter}.lnk");
                        shortcut.Description = "Picture shortcut for screensaver";
                        shortcut.TargetPath = filePath;
                        shortcut.Save();
                        shortcutCounter++;

                        if (DateTime.Now.Second != lastSecond)
                        {
                            dotCount++;
                            if (dotCount > 3)
                                dotCount = 0;
                            InfoLabel.Content = "Loading" + Enumerable.Repeat(".", dotCount);
                        }
                    }
                    catch (Exception e)
                    {
                        errorCounter++;
                    }
                }
                InfoLabel.Content =
                    $"Successfully converted {shortcutCounter} pictures. {errorCounter} errors";
            }
            catch (Exception e)
            {
                InfoLabel.Content = e.ToString();
            }
            ExecuteButton.IsEnabled = true;
        }
        
        private void UpdateCountLabel()
        {
            var count = FileIO.FileCount(_pathData.FamilyPhotosPath);
            InfoLabel.Content = $"{count} files found";
        }
        
        private void RootPathBrowseButton_OnClick(object sender, RoutedEventArgs e)
        {
            FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();
            folderBrowserDialog.ShowNewFolderButton = true;
            
            DialogResult result = folderBrowserDialog.ShowDialog();
            if (result == System.Windows.Forms.DialogResult.OK && Directory.Exists(folderBrowserDialog.SelectedPath))
            {
                _pathData.FamilyPhotosPath = folderBrowserDialog.SelectedPath;
                RootPathTextBox.Text = _pathData.FamilyPhotosPath;
                UpdateCountLabel();
                JsonManager.WriteJson(_pathData);
            }
        }

        private void DestPathBrowseButton_OnClick(object sender, RoutedEventArgs e)
        {
            FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();
            folderBrowserDialog.ShowNewFolderButton = true;
            
            DialogResult result = folderBrowserDialog.ShowDialog();
            if (result == System.Windows.Forms.DialogResult.OK && Directory.Exists(folderBrowserDialog.SelectedPath))
            {
                _pathData.DestinationPath = folderBrowserDialog.SelectedPath;
                DestPathTextBox.Text = _pathData.DestinationPath;
                JsonManager.WriteJson(_pathData);
            }
        }
        
        private void ExecuteButton_OnClick(object sender, RoutedEventArgs e)
        {
            CreateShortcuts();
        }
    }
}