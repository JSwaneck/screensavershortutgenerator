﻿using System;
using System.Linq;
using System.Windows;
using FileManager;

namespace ScreensaverShortcutGenerator
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            if (e.Args.Any(i => i.ToLower() == "shuffle"))
            {
                var jsonData = FileManager.JsonManager.GetJson();
                if (jsonData == null)
                    return;
                var destPath = jsonData.Value.DestinationPath;
                FileIO.ShuffleDest(destPath);
                base.Shutdown();
                return;
            }
            base.OnStartup(e);
        }
    }
}